import json

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Categories
from .models import Carts
from .models import Products
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.conf import settings

def index(request):
    if request.method == 'POST':
        query = request.POST['q']
        if query != '':
            return HttpResponseRedirect('/to2compras/queryResults?q='+query+'&category=')
    if request.method == 'GET':
        categories_list = Categories.objects.all().order_by('description')
        products_with_img_list = Products.objects.all()[:3]
        products_list = Products.objects.all()[3:9]
        products_list = Products.objects.all()
        context = {'categories': categories_list, 'products_with_img': products_with_img_list, 'products_list': products_list}
        return render(request, 'to2compras/index.html', context)


def about(request):
    categories_list = Categories.objects.all().order_by('description')
    context = {'categories': categories_list }
    return render(request, 'to2compras/about.html', context)

def cart(request):
    user = request.user
    categories_list = Categories.objects.all().order_by('description')
    cart_products_list = Carts.objects.all().filter(user_id=user.id)
    if 'action' in request.POST:
        action = request.POST['action']
        if action == 'cleanCart':
            Carts.objects.filter(user_id=user.id).delete()
            return HttpResponseRedirect('/to2compras/cart')
        if action == 'deleteSingleProduct':
            product_id_to_delete =  request.POST['productToDelete']
            Carts.objects.filter(user_id=user.id, products_id=product_id_to_delete).delete()
            return HttpResponseRedirect('/to2compras/cart')
    context = { 'cartProducts': cart_products_list, 'categories': categories_list, 'user': user }
    return render(request, 'to2compras/shopCart.html', context)


def userLogin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/to2compras')
        return render(request, 'to2compras/login.html')
    if request.method == 'GET':
        categories_list = Categories.objects.all().order_by('description')
        context = {'categories': categories_list}
        return render(request, 'to2compras/login.html', context)

def userLogout(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            logout(request)
            return HttpResponseRedirect('/to2compras')
    if request.method == 'GET':
        categories_list = Categories.objects.all().order_by('description')
        context = {'categories': categories_list}
        return render(request, 'to2compras/logout.html', context)

def product(request, id):
    categories_list = Categories.objects.all().order_by('description')
    product = Products.objects.get(id=id)
    if request.method == 'POST':
        if request.user.is_authenticated:
            newProduct = Carts.objects.create(user=request.user,products=product)
            newProduct.save()
            return HttpResponseRedirect('/to2compras/cart')
        else:
            return HttpResponseRedirect('/to2compras/login')
    if request.method == 'GET':
        context = { 'categories': categories_list, 'product': product }
        return render(request, 'to2compras/product.html', context)

def productCreate(request):
    if request.method == 'GET':
        categories_list = Categories.objects.all().order_by('description')
        context = { 'categories': categories_list }
        return render(request, 'to2compras/productCreate.html', context)
    if request.method == 'POST':
        title = request.POST['title']
        categoryId = request.POST['category']
        description = request.POST['description']
        price = request.POST['price']
        image = 'images/' + request.POST['image']
        print(request.FILES)
        cat = Categories(id=categoryId)
        Products.objects.create(category=cat, description=description, image=image, price=price, title=title)
        return HttpResponseRedirect('/to2compras/product/create')

def productEdit(request,id):
    if request.method == 'GET':
        categories_list = Categories.objects.all().order_by('description')
        product = Products.objects.get(id=id)
        print(product)
        context = { 'categories': categories_list, 'product': product}
        return render(request, 'to2compras/productEdit.html', context)
    if request.method == 'POST':
        title = request.POST['title']
        categoryId = request.POST['category']
        description = request.POST['description']
        price = request.POST['price']
        image = 'images/' + request.POST['image']
        cat = Categories(id=categoryId)
        Products.objects.create(category=cat, description=description, image=image, price=price, title=title)
        return HttpResponseRedirect('/to2compras/product/create')


def queryResults(request):
    categories_list = Categories.objects.all().order_by('description')
    category = request.GET['category']
    q = request.GET['q']
    if category != '':
        products_list = (Products.objects.filter(category__description=category))

    if q != '':
        products_list = (Products.objects.filter(title__contains=q))
    context = { 'categories': categories_list, 'products': products_list }
    return render(request, 'to2compras/queryResults.html', context)


def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        user = User.objects.create_user(username, email, password)
        user.save()
        return HttpResponseRedirect('/to2compras/registerSuccess')
    if request.method == 'GET':
        categories_list = Categories.objects.all().order_by('description')
        context = {'categories': categories_list}
        return render(request, 'to2compras/register.html', context)
