from django.db import models
from django.contrib.auth.models import User


class Categories(models.Model):
    description = models.CharField(max_length=200)


class Products(models.Model):
    category = models.ForeignKey(Categories, on_delete=models.CASCADE)
    description = models.CharField(max_length=500)
    image = models.ImageField(default='', blank=True, upload_to='images')
    price = models.IntegerField()
    title = models.CharField(max_length=200)

class Carts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    products = models.ForeignKey(to=Products, on_delete=models.CASCADE)
