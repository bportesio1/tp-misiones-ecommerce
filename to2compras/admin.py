from django.contrib import admin

from .models import Categories
from .models import Products

admin.site.register(Categories)
admin.site.register(Products)
