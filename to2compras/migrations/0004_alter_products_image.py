# Generated by Django 3.2.5 on 2021-07-08 19:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('to2compras', '0003_alter_products_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='image',
            field=models.ImageField(blank=True, default='', upload_to='images'),
        ),
    ]
