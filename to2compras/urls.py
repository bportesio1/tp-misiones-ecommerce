from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('cart', views.cart, name='cart'),
    path('login', views.userLogin, name='userLogin'),
    path('logout', views.userLogout, name='userLogout'),
    path('product/<int:id>', views.product, name='product'),
    path('product/<int:id>/edit', views.productEdit, name='product'),
    path('product/create', views.productCreate, name='productCreate'),
    path('queryResults', views.queryResults, name='queryResults'),
    path('register', views.register, name='register'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

