from django.apps import AppConfig


class To2ComprasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'to2compras'
